+++
title = "Firmware Customization"
description = "Customizing the Zaphod keymap/firmware."
date = 2021-05-01T08:20:00+00:00
updated = 2021-05-01T08:20:00+00:00
draft = false
weight = 60
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Customizing the Zaphod keymap/firmware."
toc = true
top = false
images = ["heart-of-gold.png"]
+++

## Overview

Although [ZMK](https://zmk.dev/) does not yet have GUI keymap update software, updating the keymap can be done without installing
any local build tools or toolchain. This step will walk you through setting up your personal keymap/configuration.

## Prerequisites

An understanding of the high level [ZMK keymaps](https://zmk.dev/docs/features/keymaps/) is helpful in preparing to customize your Zaphod keymap.

To have your new Zaphod firmware built automatically for you, you will need to create a [GitHub account](https://github.com/join) to manage your personal repo.

## ZMK Config Repo

After logging into your GitHub account, open [https://github.com/petejohanson/zaphod-config/](https://github.com/petejohanson/zaphod-config/) in your browser. In the top right corner, there is a button labeled "Fork":

{{ image(src="fork-button.png",alt="Fork Button") }}

Once the fork is prepared, you should be presented with a list of files in your repository:

{{ image(src="repo-listing.png", alt="Repo Listing") }}

Clicking the `boards/arm/zaphod/` link brings you to the full listing of files for the Zaphod:

{{ image(src="board-directory.png", alt="Board Directory") }}

The `zaphod.keymap` file contains your keymap:

{{ image(src="view-keymap.png", alt="View Keymap")}}

## Enabling GitHub Actions

Before making further changes, click the "Actions" link in your fork. When presented with the option, click "I understand my workflows, go ahead and run them" button.

## Keymap Editing

From the keymap file page, clicking the small pencil icon will bring you to the web editor and let you make changes to your keymap:

{{ image(src="edit-keymap.png", alt="Edit Keymap")}}

Once happy with your changes, enter a commit message describing them and click the commit button:

{{ image(src="commit.png", alt="Commit Changes")}}

## Download & Install

After committing the changes click the "Actions" link along the top to see the list of GitHub Actions builds. Find the new build in the listing, and click on the description:

{{ image(src="latest-build.png", alt="Latest Build")}}

From there, near the bottom of the screen will be download labeled "firmware", that you should click to download:

{{ image(src="firmware-download-link.png", alt="Firmware Download Link")}}

Once downloaded, unzip the downloaded file, and you will have a file, named `zaphod.uf2`.

Connect your Zaphod to your computer using a USB cable, and double tap the reset button on the top edge of the board. The blue LED should flash, slowing down once detected and connected to your computer.

A new USB "drive" should appear on your connected computer, with a name of "HEARTOFGOLD". Copy and paste the `zaphod.uf2` images to the new USB drive, and Zaphod should complete flashing the new image and instantly restart into it.

{{ image(src="heart-of-gold.png", alt="USB Drive")}}