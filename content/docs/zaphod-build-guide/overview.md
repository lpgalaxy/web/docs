+++
title = "Overview"
description = "Zaphod, a low profile, 34-key, wireless, unibody keyboard"
date = 2021-05-01T08:00:00+00:00
updated = 2021-05-01T08:00:00+00:00
draft = false
weight = 10
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Zaphod is a low profile, 34-key, wireless, unibody keyboard. This guide will walk you through building it."
toc = true
top = false
+++

## Build Guide

The Zaphod build guide will walk you through all the steps required to get your Zaphod kit fully assembled and working. 

## Soldering

If you have not soldered before, take some time to review some guides on proper soldering technique:

* [Adafruit Guide to Excellent Soldering](https://learn.adafruit.com/adafruit-guide-excellent-soldering/)
* [How To Solder: A Beginner’s Guide](https://www.makerspaces.com/how-to-solder/)

## Resources

If you are new to ZMK, check the [ZMK docs](https://zmk.dev/docs) and join the [Discord server](https://zmk.dev/community/discord/invite).

Need help with anything? Hop on the `#gb-zaphod` channel on the [Low Profile Keyboards Discord](https://discord.gg/qUVC3zmf) and ask away!
