+++
title = "Typing Test"
description = "Time to take Blank Slate for a drive."
date = 2022-08-04T08:20:00+00:00
updated = 2022-08-04T08:20:00+00:00
draft = false
weight = 55
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "With Blank Slate fully assembled, it's time to test it out!"
toc = true
top = false
images = ["layout.png"]
+++

## Default Keymap

Blank Slate ships with the following default keymap:

![Default Layout](default-keymap.svg)

### Layers

Layers are used to access other behaviors and keycodes that are not present on the base layer.

#### Base Layer

The base layer contains all the normal alpha numerics and a few common punctuation used for typing normal prose. In addition, two dedicated layer keys on the thumbs are used to access additional layers.

#### Number Layer

The number layer is activating by holding the "NUM" key on the right hand. It is used to access the number keys, as well as some additional symbols on the left hand.

#### Navigation Layer

The media layer is activating by holding the "NAV" key with the left thumb. It is largely used for text navigation, and to control bluetooth profile selection, and clear the current bluetooth profile.

To learn more, see the [ZMK Bluetooth Behavior Docs](https://zmkfirmware.dev/docs/behaviors/bluetooth).

#### Symbol Layer

The symbol layer is activating by holding the NAV and NUM thumb keys simultaneously. It is used to access the symbol keys on the left hand, to avoid needing to *also* hold a shift key on the number layer.

## USB

With Blank Slate plugged in, you should be able to start typing and verify that the keyboard is detected and working properly.

## Wireless

ZMK will automatically start advertising to allow new hosts to connect. From your computer, go to the bluetooth preferences and add a device. You should see a device named "Blank Slate" in the list. Click that item to connect. Once connected over bluetooth, you should be able to now unplug Blank Slate from USB, and start typing over BLE.

To use more than one host/device with you Blank Slate, you'll need to use differint profiles to manage the
connection/communication with each device. To learn more, read up on ZMK's [bluetooth feature](https://zmk.dev/docs/features/bluetooth).
