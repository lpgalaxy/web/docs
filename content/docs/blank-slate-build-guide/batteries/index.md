+++
title = "Battery"
description = "Installing lithium-polymer (LiPo) battery for Blank Slate"
date = 2022-08-04T08:20:00+00:00
updated = 2022-08-04T08:20:00+00:00
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Installing lithium-polymer (LiPo) batteries for Blank Slate"
toc = true
top = false
images = ["battery-installed.jpg"]

+++

## Overview

Blank Slate has charging circuitry to properly and safely charge batteries and is designed to work with 3.7v lithium-polymer (LiPo) batteries. Blank Slate will charge batteries at 100mA when power is supplied via the USB port and will stop charging when the battery is fully charged.

## Specs

The following specifications will help ensure you select an appropriate battery to use with Blank Slate. 

### Size/Dimensions

The following dimensions are for a battery that can fit within the battery cutout provided by Blank Slate, and
assuming use in a tray mount case. Use of a case with more space below the PCB may allow for a thicker battery,
but the maximums listed here should result in a battery compatible with all cases.

Note: Use of thick plates or plate foam will reduce the allowed height of the battery. Be sure *never* to compress batteries within their installed location.

<dl>
  <dt>Height</dt>
  <dd>4mm</dd>
  <dt>Width</dt>
  <dd>12.5mm</dd>
  <dt>Length</dt>
  <dd>51mm</dd>
</dl>


<aside>Always take the proper precautions when handling the LiPo battery. Failure to do so can cause permanent damage to the board or yourself or start a fire. 

* Check the battery for damage, leaks, or swelling -- do NOT use the battery if any issues are found.
* Check the orientation of the wires, e.g., red and black.
* Do NOT allow the exposed ends of the red and black wires to touch.
</aside>

### Connector

The Blank Slate includes two possible connectors to make connecting and disconnecting batteries easy.

* On the bottom side of the PCB, a JST ACH 1.0mm socket is available.
* On the top side of the PCB, where there is more room, a JST SH 1.0mm socket is available.

Should you be unable to purchase a battery with one of the above connectors, the PCB offers plated through holes for
directly soldering a battery.

When the JST plug is oriented with the alignment ridge on top of the connector and the wires extending away from you, as pictured, then the black wire should be on the right, and the red wire should be on the left.

### JST ACH Polarity

The following demonstrates a battery with a JST ACH battery with the correct polarity:

{{ image(src="jst-ach-polarity-example.jpg",alt="JST ACH Polarity") }}

### JST SH Polarity

The following demonstrates a battery with a JST SH battery with the correct polarity:

{{ image(src="jst-sh-battery-example.jpg",alt="JST ACH Polarity") }}

## Purchasing

Due legal and shipping constraints, batteries may not be available for purchase with with your PCB.

The ideal LiPo battery suitable for Blank Slate that will fit in the case without being compressed is the `401250` LiPo, which should have a 240-300 mAh capacity. If purchasing a battery separately, the following are recommended:

* Dimensions for the battery to fit within the case cavity: 4.0mm x 12mm x 50mm.
* JST ACH or SH 1.0mm socket to allow you to easily plug and unplug the battery.

If the vendor providing you the Blank Slate PCB was unable to offer batteries as well, one known working source of batteries is https://www.aliexpress.com/item/1005006166119878.html?spm=a2g0o.order_list.order_list_main.31.45aa1802CYxaa6

## Installation

Installation steps will vary, depending on the battery connectors.

### JST Connector

If you have purchased a battery with a JST connector, first double check that the red and black wires are on the correct side. There is a silk label in front of the JST sockets on Blank Slate to remind you which wire should be on which side when attaching the plug.


#### JST ACH (PCB Bottom)

Gently insert the JST plug into the socket from *above*. Gently press down on the plug with your fingertip. It should snap fully into the socket:

{{ image(src="jst-ach-battery-installed.jpg",alt="JST ACH fully installed in the PCB") }}

#### JST SH (PCB Top)

On the top side of the PCB is a JST SH connector. Gently insert the battery from the side, pressing it in while supporting the back of the connector on the PCB, to avoid pushing too hard and damaging the PCB.


TODO: Picture of inserting a battery on the top of the PCB

### Bare Wires

If you are soldering a bare wire battery to Blank Slate, *do not allow the exposed ends of the red and black wires to touch as this may cause the battery to explode.** Most batteries without a JST socket will come with the positive red wire covered in tape. Be sure to leave that tape on until you are ready to solder that wire.

1. Solder the black wire to the pin on the Blank Slate PCB labeled "-". It should the bottom pin when looking at the Blank Slate when it is oriented like normal.
2. Check to ensure there is no short from the black wire/pad to the red/positive pad next to it on the Blank Slate PCB. 
3. Remove the tape from the red positive lead of the battery, being careful to prevent the red wire from touching the ground or any other part of the Blank Slate PCB connections.
4. Carefully solder the red positive battery wire to the pin on Blank Slate labeled "+".
5. Orient the battery with the leads where the switch plate is labeled "Leads Here", and attach the battery to the double sided tape applied to the bottom of the swith plate, to keep the battery in place.

## In-Switch Status LED

Blank Slate includes an orange status LED within the 2nd switch of the top row that is used to indicate various charging information:

<dl>
  <dt>Off</dt>
  <dd>Battery is discharging, or no USB power is being supplied to charge the battery.</dd>
  <dt>Blinking</dt>
  <dd>USB power supplied, but no battery connected/detected.</dd>
  <dt>Solid (Bright)</dt>
  <dd>USB power supplied, and the battery is actively being charged.</dd>
  <dt>Solid (Dim)</dt>
  <dd>USB power supplied, but the battery is fully charged.</dd>
</dl>
