+++
title = "Firmware Customization"
description = "Customizing the Technikable keymap/firmware."
date = 2022-08-04T08:20:00+00:00
updated = 2022-08-04T08:20:00+00:00
draft = false
weight = 60
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Customizing the Technikable keymap/firmware."
toc = true
top = false
images = ["heart-of-gold.png"]
+++

## Overview

Although [ZMK](https://zmk.dev/) does not yet have GUI keymap update software, updating the keymap can be done without installing
any local build tools or toolchain. This step will walk you through setting up your personal keymap/configuration.

## Prerequisites

An understanding of the high level [ZMK keymaps](https://zmk.dev/docs/features/keymaps/) is helpful in preparing to customize your Technikable keymap.

To have your new Technikable firmware built automatically for you, you will need to create a [GitHub account](https://github.com/join) to manage your personal repo.

## ZMK Config Repo

After logging into your GitHub account, you'll need to fork the correct repository for the bottom row layout you've assembled:

* Full ortho bottom row: [https://github.com/petejohanson/technikable-ortho-config](https://github.com/petejohanson/technikable-ortho-config)
* MIT/single 2u: [https://github.com/petejohanson/technikable-mit-config](https://github.com/petejohanson/technikable-mit-config)
* Dual 2u: [https://github.com/petejohanson/technikable-dual-2u-config](https://github.com/petejohanson/technikable-dual-2u-config)

Once you open the correct link, in the top right corner, click the button labeled "Fork":

{{ image(src="fork-button.png",alt="Fork Button") }}

Once the fork is prepared, you should be presented with a list of files in your repository:

{{ image(src="repo-listing.png", alt="Repo Listing") }}

Clicking the `config/` link brings you to the full listing of files for the Technikable:

{{ image(src="config-directory.png", alt="Config Directory") }}

The `technikable.keymap` file contains your keymap:

{{ image(src="view-keymap.png", alt="View Keymap")}}

## Enabling GitHub Actions

Before making further changes, click the "Actions" link in your fork. When presented with the option, click "I understand my workflows, go ahead and run them" button.

## Keymap Editing

From the keymap file page, clicking the small pencil icon will bring you to the web editor and let you make changes to your keymap:

{{ image(src="edit-keymap.png", alt="Edit Keymap")}}

Once happy with your changes, enter a commit message describing them and click the commit button:

{{ image(src="commit.png", alt="Commit Changes")}}

## GUI Keymap Editor

Alternatively, you can use Nick's amazing [Keymap Editor](https://nickcoutsos.github.io/keymap-editor/) with your newly created repo and it will "just work".

## Download & Install

After committing the changes click the "Actions" link along the top to see the list of GitHub Actions builds. Find the new build in the listing, and click on the description:

{{ image(src="latest-build.png", alt="Latest Build")}}

From there, near the bottom of the screen will be download labeled "firmware", that you should click to download:

{{ image(src="firmware-download-link.png", alt="Firmware Download Link")}}

Once downloaded, unzip the downloaded file, and you will have a two files, named `technikable-zmk.uf2` and `technikable@1.1-zmk.uf2`. Use `technikable@1.1-zmk.uf2` if you have the latest v1.1 PCBs, otherwise use `technikable-zmk.uf2`

Connect your Technikable to your computer using a USB cable, and double tap the reset button through the small hole on the bottom of the case. The blue LED should flash, slowing down once detected and connected to your computer.

A new USB "drive" should appear on your connected computer, with a name of "TECHNIKABLE". Copy and paste the correct UF2 image to the new USB drive, and Technikable should complete flashing the new image and instantly restart into it.
